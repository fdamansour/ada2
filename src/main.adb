
with Ada.Text_IO;
with Ada.Integer_Text_IO;
procedure Main is
   -- voyages au Canada : -20% sur 860e d'avion et 7j � 48e/j.
   type tpromo is digits 3 range 0.0 .. 100.0;
   promo : tpromo := 20.0; -- appliqu� � l'ensemble du prix
   type tprix is digits 6 range 0.0 .. 10000.0;
   avion : tprix := 860.0;
   hotel : tprix := 48.0;
   type tduree is range 0 .. 100;
   duree : tduree := 7;
   duree_i : integer;
   total : tprix;
   type tjours is (lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche);
   type tweekend is (samedi, dimanche);
   type tjsemaine is new tjours range lundi .. vendredi;
   subtype tgrandweekend is tjours range vendredi .. dimanche;
   subtype tgdweekend is tgrandweekend;
   depart : tjours := samedi;
   arrivee : tgrandweekend := dimanche;
   voldenuit : boolean := depart /= arrivee;
   infos : string := "Voyages au Canada : -" & promo'Image &
                          " % sur " & avion'Image & "e d'avion et " &
     duree'Image & "jours a " & hotel'Image & "e";
   infostotal : string (1 .. 28);
   voyage_moyen:boolean := duree > 3 and then duree < 10;
begin
   Ada.Text_IO.Put_line("Bienvenue dans l'agence de voyage");
   Ada.Text_IO.Put_line("Jours ?");
   Ada.Integer_Text_IO.Get(duree_i);
   duree := tduree(duree_i);
   Ada.Text_IO.Put_line(infos);
   -- corriger : calculer le total complet
   total := tprix(float(hotel) * float(duree) + float(avion));
   total := total * tprix(1.0 - float(promo) / 100.0);
   infostotal := "Total Canada : " & total'Image & "e";
   Ada.Text_IO.Put_line(infostotal);
   Ada.Text_IO.Put_line("Depart le : " & depart'Image & ".");
   Ada.Text_IO.Put_line("Depart dimanche ? " & boolean(depart=dimanche)'Image & ".");
   -- En g�n�ral : Air Canada ;
   -- Voyages court (<7) : Quebec'Air
   -- Voyage de 7, 14, 21, 28j : Can'jet
   case duree is
      when 0 .. 6 =>  Ada.Text_IO.Put_line("Quebec'Air");
      when 7 | 14 | 21 | 28 =>  Ada.Text_IO.Put_line("Can'Jet");
      when others =>  Ada.Text_IO.Put_line("Air Canada");
   end case;
   -- Ada 2012+ seulement :
   Ada.Text_IO.Put_line( if duree > 45 then "Visa !" else "" );

   for Variable in 1.. duree loop
      if variable=1 or variable=duree then
         Ada.Text_IO.Put_Line ("Jour" & Variable'Image & " :" & "avion");
      elsif (variable-1) mod 4 = 0 then
         Ada.Text_IO.Put_Line ("Jour" & Variable'Image & " :" & "repos");
      else
         Ada.Text_IO.Put_Line ("Jour" & Variable'Image & " :" & "marche");
      --Ada.Text_IO.Put_Line ("Jour" & Variable'Image & " :");
      end if;
   end loop;


end Main;
